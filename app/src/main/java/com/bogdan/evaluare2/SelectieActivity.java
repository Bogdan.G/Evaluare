package com.bogdan.evaluare2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class SelectieActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final Integer LOCATION = 0x1;
    static final Integer GPS_SETTINGS = 0x7;
    public static String theLocation;
    public static Location location;
    private static String tipEvaluare = null;
    protected LocationManager locationManager;
    GoogleApiClient client;
    LocationRequest mLocationRequest;
    PendingResult<LocationSettingsResult> result;
    private EditText editTextSelectie;
    private RadioButton radioButton4;
    private double longitude = 0.0;
    private double latitude = 0.0;
    private List<Address> locationEvaluate;
    private boolean evaluare4 = false;
    private boolean isGPSEnabled;
    private boolean isNetworkEnabled;
    private static boolean isConnected;

    public static String getTipEvaluare() {
        return tipEvaluare;
    }

    public static void setTipEvaluare(String string) {
        tipEvaluare = string;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the content of the activity to use the activity_selectie.xml layout file
        setContentView(R.layout.activity_selectie);

        editTextSelectie = (EditText) findViewById(R.id.editTextTipEvaluare);
        radioButton4 = (RadioButton) findViewById(R.id.evaluare4);

        editTextSelectie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioButton4.setChecked(true);
            }
        });

        editTextSelectie.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        //set up a button for the user to access the next activity(FormularActivity)
        Button toFormular = (Button) findViewById(R.id.toFormularButton);
        toFormular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (evaluare4) {
                    setTipEvaluare(editTextSelectie.getText().toString());
                }
                ask();
            }
        });

        // Create an instance of GoogleAPIClient.
        if (client == null) {
            client = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

    } //end onCreate

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(SelectieActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(SelectieActivity.this, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(SelectieActivity.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(SelectieActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            validateGoToIntent();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        client.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        client.disconnect();
    }

    private void askForGPS() {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(client, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(SelectieActivity.this, GPS_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });

        validateGoToIntent();

    } //end askForGPS

    private void ask() {
        if (Build.VERSION.SDK_INT >= 23) {
            askForPermission(android.Manifest.permission.ACCESS_FINE_LOCATION, LOCATION);
        } else {
            validateGoToIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            askForGPS();
        }
    }

    public void validateGoToIntent() {

        if ((ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED)) {

            return;
        }

        ConnectivityManager cm =
                (ConnectivityManager)SelectieActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (!isConnected) {
            Toast.makeText(getApplicationContext(), "Vă rugăm să vă asigurați că aveți conexiune activă la internet.", Toast.LENGTH_LONG).show();
            return;
        }

        locationManager = (LocationManager) SelectieActivity.this.getSystemService(LOCATION_SERVICE);

        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (isGPSEnabled) {

            location = LocationServices.FusedLocationApi.getLastLocation(client);
        }

        if (isNetworkEnabled && (location == null)) {

            if (locationManager != null) {
                location = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
        }

        if (location != null) {

            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }

        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        try {

            locationEvaluate = geocoder.getFromLocation(latitude, longitude, 1);
            if (null != locationEvaluate && locationEvaluate.size() > 0) {
                theLocation = locationEvaluate.get(0).getAddressLine(0)
                        .concat(", " + locationEvaluate.get(0).getAddressLine(1))
                        .concat(", " + locationEvaluate.get(0).getAddressLine(2));
            }

        } catch (IOException e) {

        }

        if(getTipEvaluare() != null) {

            Intent photoIntentActivity = new Intent(SelectieActivity.this, FormularActivity.class);
            startActivity(photoIntentActivity);

        } else {

            Toast.makeText(getApplicationContext(), "Vă rugăm să selectați tipul evaluării.", Toast.LENGTH_SHORT).show();

        }

    }

    // Select the type of evaluation
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        editTextSelectie.setCursorVisible(false);
        editTextSelectie.clearFocus();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.evaluare1:
                if (checked) {
                    setTipEvaluare(getResources().getString(R.string.evaluare1));
                }
                break;
            case R.id.evaluare2:
                if (checked) {
                    setTipEvaluare(getResources().getString(R.string.evaluare2));
                }
                break;
            case R.id.evaluare3:
                if (checked) {
                    setTipEvaluare(getResources().getString(R.string.evaluare3));
                }
                break;
            case R.id.evaluare4:
                if (checked) {
                    editTextSelectie.setCursorVisible(true);
                    evaluare4 = true;
                }
                break;
        } //end switch
    } //end onRadioButtonClicked

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onConnected(Bundle connectionHint) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
} //end MainActivity
