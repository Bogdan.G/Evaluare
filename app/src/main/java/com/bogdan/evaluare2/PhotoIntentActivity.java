package com.bogdan.evaluare2;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.bogdan.evaluare2.PhotoFragment.mListener;
import static com.bogdan.evaluare2.PhotoOptionsFragment.mCurrentPosition;

/**
 * This class contains the two fragments: {@link PhotoFragment}
 * and {@link PhotoOptionsFragment}, responsible
 * with the capture of photos meant to be evaluated
 */

public class PhotoIntentActivity extends AppCompatActivity
        implements View.OnClickListener {

    //Declare the variables used
    public static final int REQUEST_TAKE_PHOTO = 1;
    public static final Integer CAMERA = 0x5;
    public static Uri[] photoURI = new Uri[6];
    public static String[] mPhoto = {"", "", "", "", "", ""};
    public static Map<View, Integer> map;
    public static int imageViewNumber = 0;
    public static File[] photoFile = new File[6];
    public static File[] file = new File[6];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(getApplicationContext());
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());

        setContentView(R.layout.activity_photo);

        // Check that the activity is using the layout version with
        // the container FrameLayout
        if (findViewById(R.id.container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            PhotoFragment firstFragment = new PhotoFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, firstFragment).commit();
        } //end if statement

    } //end onCreate

    //Create the image file needed to store the photos
    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = this.getFilesDir();
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    } //end createImageFile

    //The Camera Activity
    public void dispatchTakePictureIntent() {

        if (imageViewNumber <= 5) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity((PhotoIntentActivity.this).getPackageManager()) != null) {
                // Create the File where the photo should go
                photoFile[imageViewNumber] = null;
                try {
                    photoFile[imageViewNumber] = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile[imageViewNumber] != null) {
                    photoURI[imageViewNumber] = FileProvider.getUriForFile(PhotoIntentActivity.this,
                            "com.bogdan.evaluare2.fileprovider",
                            photoFile[imageViewNumber]);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI[imageViewNumber]);
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

                    mPhoto[imageViewNumber] = photoURI[imageViewNumber].toString();
                }
            }
        }
    } //end dispatchTakePictureIntent

    //After the photo is taken display the image and setOnClickListener

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.ic_placeholder)
                    .showImageOnFail(R.drawable.ic_error)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();

            ImageView imageView1 = (ImageView) findViewById(R.id.imageViewPhoto1);
            ImageView imageView2 = (ImageView) findViewById(R.id.imageViewPhoto2);
            ImageView imageView3 = (ImageView) findViewById(R.id.imageViewPhoto3);
            ImageView imageView4 = (ImageView) findViewById(R.id.imageViewPhoto4);
            ImageView imageView5 = (ImageView) findViewById(R.id.imageViewPhoto5);
            ImageView imageView6 = (ImageView) findViewById(R.id.imageViewPhoto6);

            ImageView[] imageViews = {imageView1, imageView2, imageView3, imageView4, imageView5, imageView6};

            imageLoader.displayImage(mPhoto[imageViewNumber], imageViews[imageViewNumber], options);
            imageViews[imageViewNumber].setOnClickListener(mListener);

            imageViewNumber++;

        } //end if statement

    } //end onActivityResult

    @Override
    public void onClick(View v) {

        // The user selected the imageView from PhotoFragment
        ImageView imageView1 = (ImageView) findViewById(R.id.imageViewPhoto1);
        ImageView imageView2 = (ImageView) findViewById(R.id.imageViewPhoto2);
        ImageView imageView3 = (ImageView) findViewById(R.id.imageViewPhoto3);
        ImageView imageView4 = (ImageView) findViewById(R.id.imageViewPhoto4);
        ImageView imageView5 = (ImageView) findViewById(R.id.imageViewPhoto5);
        ImageView imageView6 = (ImageView) findViewById(R.id.imageViewPhoto6);

        map = new HashMap<>();
        map.put(imageView1, 0);
        map.put(imageView2, 1);
        map.put(imageView3, 2);
        map.put(imageView4, 3);
        map.put(imageView5, 4);
        map.put(imageView6, 5);

        int position = map.get(v);

        // Capture the options_container from the activity layout
        PhotoOptionsFragment photoOptionsFragment = (PhotoOptionsFragment)
                getSupportFragmentManager().findFragmentById(R.id.options_container);

        if (photoOptionsFragment != null) {

            // Call a method in the PhotoOptionsFragment to update its content
            photoOptionsFragment.updateImageView(position);

        } else {
            // If the frag is not available, we're in the one-pane layout and must swap frags...

            // Create fragment and give it an argument for the selected photo
            PhotoOptionsFragment newFragment = new PhotoOptionsFragment();
            Bundle args = new Bundle();
            args.putInt(PhotoOptionsFragment.ARG_POSITION, position);
            newFragment.setArguments(args);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        } //end if/else statement
    } //end OnClick

    // The user clicked the Cancel button, return to PhotoFragment
    public void revocare() {

        PhotoFragment photoFragment = new PhotoFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.container, photoFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();

    } //end revocare

    /* The user clicked the Delete button, delete the photo
    and rearrange the photos taken */
    public void stergere() {

        PhotoFragment photoFragment = new PhotoFragment();

        for (int n = mCurrentPosition; n <= 4; n++) {
            if (!(mPhoto[n + 1]).isEmpty()) {
                mPhoto[n] = mPhoto[n + 1];
            } else {
                mPhoto[n] = "";
            }
        }
        mPhoto[5] = "";
        if (imageViewNumber != 0) {
            imageViewNumber--;
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.container, photoFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    } //end stergere

    public void ask() {
        if (Build.VERSION.SDK_INT >= 23) {
            askForPermission(android.Manifest.permission.CAMERA, CAMERA);
        } else {
            dispatchTakePictureIntent();
        }
    }

    private void askForPermission(String permission, Integer requestCode) {

        if (ContextCompat.checkSelfPermission(PhotoIntentActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(PhotoIntentActivity.this, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(PhotoIntentActivity.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(PhotoIntentActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            dispatchTakePictureIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            dispatchTakePictureIntent();
        }
    }

}  //end Activity



