package com.bogdan.evaluare2;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

import static com.bogdan.evaluare2.FormularActivity.documente;
import static com.bogdan.evaluare2.FormularActivity.locatie;
import static com.bogdan.evaluare2.FormularActivity.mail;
import static com.bogdan.evaluare2.FormularActivity.nume;
import static com.bogdan.evaluare2.FormularActivity.structura;
import static com.bogdan.evaluare2.FormularActivity.telefon;
import static com.bogdan.evaluare2.PhotoIntentActivity.imageViewNumber;
import static com.bogdan.evaluare2.PhotoIntentActivity.mPhoto;
import static com.bogdan.evaluare2.PhotoIntentActivity.photoFile;
import static com.bogdan.evaluare2.SelectieActivity.setTipEvaluare;
import static com.bogdan.evaluare2.SelectieActivity.theLocation;


/**
 * Present a summary of the user's data
 */

public class SummaryActivity extends AppCompatActivity {

    public static String textContactDetails;
    private ProgressBar progressBar;
    private static boolean isSent = false;
    private static File[] file = new File[6];
    private static InputStream is;
    private static boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the content of the activity to use the activity_summary.xml layout file
        setContentView(R.layout.activity_summary);

        // my_child_toolbar is defined in the layout file
        Toolbar myChildToolbar =
                (Toolbar) findViewById(R.id.menu_summary);
        setSupportActionBar(myChildToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);

        //Details about the user
        TextView textView = (TextView) findViewById(R.id.summary_text);

        Resources res = getResources();
        String text1 = res.getString(R.string.summary_evaluare, SelectieActivity.getTipEvaluare());
        String text2 = res.getString(R.string.summary_contact, locatie, nume, telefon, mail, documente, structura);
        textContactDetails = text1 + text2;
        textView.setText(textContactDetails);

        //The user's photos
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_placeholder)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();

        ImageView imageView1 = (ImageView) findViewById(R.id.imageViewSummary1);
        ImageView imageView2 = (ImageView) findViewById(R.id.imageViewSummary2);
        ImageView imageView3 = (ImageView) findViewById(R.id.imageViewSummary3);
        ImageView imageView4 = (ImageView) findViewById(R.id.imageViewSummary4);
        ImageView imageView5 = (ImageView) findViewById(R.id.imageViewSummary5);
        ImageView imageView6 = (ImageView) findViewById(R.id.imageViewSummary6);

        ImageView[] imageViews = {imageView1, imageView2, imageView3, imageView4, imageView5, imageView6};

        //When the View is created display the photos
        for (int n = 0; n <= 5; n++) {
            imageLoader.displayImage(mPhoto[n], imageViews[n], options);
        }

        ConnectivityManager cm =
                (ConnectivityManager)SummaryActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        //Set the button an intent for the FinalizeActivity
        Button button = (Button) findViewById(R.id.summary_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                new Thread(new Runnable() {
                    public void run() {
                        if(isConnected) {

                            try {
                                GMailSender sender = new GMailSender(
                                        "testmail.evaluare@gmail.com",
                                        "evaluare");
                                for (int n = 0; n < imageViewNumber; n++) {
                                    sender.addAttachment(resizeImg(n));
                                }
                                sender.sendMail("Test mail", textContactDetails + "\n\n" + "Locație de pe GPS: " + theLocation,
                                        "testmail.evaluare@gmail.com",
                                        "opinion.experts@gmail.com");
                                isSent = true;
                                goNext();
                            } catch (Exception e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Eroare. Vă rugăm apăsați din nou.", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                        } else {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Vă rugăm să vă asigurați că aveți conexiune activă la internet.", Toast.LENGTH_LONG).show();
                                }
                            });

                        }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                    }
                }).start(); //end thread
            } //end onClick
        }); //end onClickListener

    } //end onCreate

    //if the mail is sent clear the progressBar and go to next Activity
    public void goNext() {
        while(true) {
            if(isSent) {
                clean();
                Intent intent = new Intent(SummaryActivity.this, FinalizeActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    //set the variables to null in case the user
    //wants to run the app again
    private void clean() {
        setTipEvaluare(null);
        locatie = "";
        nume = "";
        telefon = "";
        mail = "";
        documente = "";
        structura = "";
        imageViewNumber = 0;
        for(int n = 0; n <= 5; n++) {
            mPhoto[n] = "";
        }
    }

    public String resizeImg(int viewNumber) {

        try {
            is = new FileInputStream(photoFile[viewNumber]);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap b= BitmapFactory.decodeStream(is);

        Bitmap out = Bitmap.createScaledBitmap(b, 500, 400, false);

        file[viewNumber] = new File(getFilesDir(), "resize" + viewNumber + ".png");
        FileOutputStream[] fOut = new FileOutputStream[6];
        try {
            fOut[viewNumber] = new FileOutputStream(file[viewNumber]);
            out.compress(Bitmap.CompressFormat.PNG, 100, fOut[viewNumber]);
            fOut[viewNumber].flush();
            fOut[viewNumber].close();
            b.recycle();
            out.recycle();
        } catch (Exception e) {

        }

        return file[viewNumber].getAbsolutePath();
    }

} //end SummaryActivity
