package com.bogdan.evaluare2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import static com.bogdan.evaluare2.PhotoIntentActivity.imageViewNumber;
import static com.bogdan.evaluare2.PhotoIntentActivity.mPhoto;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link android.view.View.OnClickListener} interface
 * to handle interaction events.
 * Here are the photos taken.
 */
public class PhotoFragment extends Fragment {

    public static View.OnClickListener mListener;

    public PhotoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo, container, false);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_placeholder)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();

        ImageView imageView1 = (ImageView) view.findViewById(R.id.imageViewPhoto1);
        ImageView imageView2 = (ImageView) view.findViewById(R.id.imageViewPhoto2);
        ImageView imageView3 = (ImageView) view.findViewById(R.id.imageViewPhoto3);
        ImageView imageView4 = (ImageView) view.findViewById(R.id.imageViewPhoto4);
        ImageView imageView5 = (ImageView) view.findViewById(R.id.imageViewPhoto5);
        ImageView imageView6 = (ImageView) view.findViewById(R.id.imageViewPhoto6);

        ImageView[] imageViews = {imageView1, imageView2, imageView3, imageView4, imageView5, imageView6};

        /**
         * When the view is created, display the photos,
         * if there is no photo, set the click action to take photo(dispatchTakePictureIntent),
         * else set the click action to take the user to the options for the photo
         */
        for (int n = 0; n <= 5; n++) {

            imageLoader.displayImage(mPhoto[n], imageViews[n], options);

            if(n >= imageViewNumber) {

                imageViews[n].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((PhotoIntentActivity)getActivity()).ask();
                    }
                });

            }

            else if(n < imageViewNumber) {

                imageViews[n].setOnClickListener(mListener);

                }

        }

        //The button for the Camera Intent
        Button button = (Button) view.findViewById(R.id.btnIntent);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PhotoIntentActivity)getActivity()).ask();
            }
        });

        //The button for the Finalize Intent
        Button toMail = (Button) view.findViewById(R.id.toMailButton);
        toMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageViewNumber >= 1) {
                    Intent intent = new Intent(getActivity(), SummaryActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getContext(), "Vă rugăm să introduceți cel puțin o fotografie.", Toast.LENGTH_SHORT).show();
                }
            }
        }); //end onClickListener

        return view;

    } //end onCreateView

    //Make sure that View.OnClickListener is implemented
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof View.OnClickListener) {
            mListener = (View.OnClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPhotoSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
