package com.bogdan.evaluare2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import static com.bogdan.evaluare2.PhotoIntentActivity.mPhoto;

/*
 * A Fragment to view a single photo enlarged, with options
 */
public class PhotoOptionsFragment extends Fragment {

    final static String ARG_POSITION = "position";
    public static int mCurrentPosition = -1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_photo_options, container, false);

        Button buttonStergere = (Button) view.findViewById(R.id.buttonStergere);
        Button buttonRevocare = (Button) view.findViewById(R.id.buttonRevocare);

        buttonRevocare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PhotoIntentActivity) getActivity()).revocare();
            }
        });

        buttonStergere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PhotoIntentActivity)getActivity()).stergere();
            }
        });

        // Inflate the layout for this fragment
        return view;

    } // end onCreateView

    @Override
    public void onStart() {
        super.onStart();

        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely call the method
        // below that sets the photo.
        Bundle args = getArguments();
        if (args != null) {
            // Set photo based on argument passed in
            updateImageView(args.getInt(ARG_POSITION));
        } else if (mCurrentPosition != -1) {
            // Set photo based on saved instance state defined during onCreateView
            updateImageView(mCurrentPosition);
        }
    } //end onStart

    //Function to take the photo selected and update the ImageView
    public void updateImageView(int position) {

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_placeholder)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();

        ImageView optionsView = (ImageView) getActivity().findViewById(R.id.optionsImageView);
        imageLoader.displayImage(mPhoto[position], optionsView, options);
        mCurrentPosition = position;
    } // end updateImageView
} //end PhotoOptionsFragment
