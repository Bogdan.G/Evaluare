package com.bogdan.evaluare2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Get information from the client
 */

public class FormularActivity extends AppCompatActivity {

    private EditText edit_text_locatie;
    private EditText edit_text_nume;
    private EditText edit_text_telefon;
    private EditText edit_text_mail;
    private EditText edit_text_documente;
    private EditText edit_text_structura;
    public static String locatie;
    public static String nume;
    public static String telefon;
    public static String mail;
    public static String documente;
    public static String structura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formular);

        // my_child_toolbar is defined in the layout file
        Toolbar myChildToolbar =
                (Toolbar) findViewById(R.id.menu_formular);
        setSupportActionBar(myChildToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        //Get the input provided by the user
        edit_text_locatie = (EditText) findViewById(R.id.edit_text_locatie);
        edit_text_nume = (EditText) findViewById(R.id.edit_text_nume);
        edit_text_telefon = (EditText) findViewById(R.id.edit_text_telefon);
        edit_text_mail = (EditText) findViewById(R.id.edit_text_mail);
        edit_text_documente = (EditText) findViewById(R.id.edit_text_documente);
        edit_text_structura = (EditText) findViewById(R.id.edit_text_structura);

        Button button_formular = (Button)findViewById(R.id.button_formular);
        button_formular.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the numbers View is clicked on.
            @Override
            public void onClick(View view) {
                validateGoToIntent();
            }
        });

    } //end onCreate

    public void validateGoToIntent() {

        locatie = edit_text_locatie.getText().toString();
        nume = edit_text_nume.getText().toString();
        telefon = edit_text_telefon.getText().toString();
        mail = edit_text_mail.getText().toString();
        documente = edit_text_documente.getText().toString();
        structura = edit_text_structura.getText().toString();

        String[] contact = {locatie, nume, telefon, mail, documente, structura};

        if (isValid(contact)) {
            Intent photoIntentActivity = new Intent(FormularActivity.this, PhotoIntentActivity.class);
            startActivity(photoIntentActivity);
        }
        else {
            Toast.makeText(getApplicationContext(), "Vă rugăm să completați toate câmpurile.", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isValid(String[] string) {

        boolean valid = false;
        for (String n : string) {
            if(n.length() < 1) {
                valid = false;
                break;
            }
            else {
                valid = true;
            }
        }
        return valid;
    }

} //end FormularAcitity